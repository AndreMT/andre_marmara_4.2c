<?php
					include("Conn.php");
					
					$sql = "SELECT 
								a.username,
								c.pointsPlatform ,
								c.pointsQuiz,
								c.totalscore
							FROM acclevel AS c
							INNER JOIN account AS a on a.accountID = c.accountID
							INNER JOIN levels AS b  on b.LevelID = c.levelID
							GROUP BY a.username 
							ORDER BY (c.totalscore) DESC";

					$result = mysqli_query($conn,$sql)or die(mysqli_error());

					echo "<table class='table table-condensed'>";
					echo "<thead>";
					echo "<tr>
					<th>Username</th>
					<th>Platformer Points</th>
					<th>Quiz Points</th>
					<th>Total</th>
					</tr>";
					echo "</thead>";
					echo "<tbody>";

					while($row = mysqli_fetch_array($result)) {
						$username = $row['username'];
						$platform = $row['pointsPlatform'];
						$quiz = $row['pointsQuiz'];
						$total = $row['totalscore'];
						echo "<tr>
						<td style='width: 200px;'>".$username."</td>
						<td style='width: 200px;'>".$platform."</td>
						<td style='width: 200px;'>".$quiz."</td>
						<td style='width: 200px;'>".$total."</td>
						</tr>";
					} 
					echo "</tbody>";
					echo "</table>";
					mysqli_close($conn);
?>		