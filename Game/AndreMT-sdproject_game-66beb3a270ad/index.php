<!DOCTYPE html>
<html lang= "en">		
<head>
	<meta charset="utf-8">
	<title>Dre's Story </title>
	<meta name="description" content="Travelling Light">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
    <link rel="stylesheet" href="TemplateData/style.css">
    <link rel="shortcut icon" href="TemplateData/favicon.ico" />
    <script src="TemplateData/UnityProgress.js"></script>
	
</head>
<body>
	<!-- Navigation -->
	<nav class= "navbar navbar-inverse navbar-fixed-top" id="nav">
		<div class= "container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 				
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					
				</button>
								
			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse">		
				<ul class ="nav navbar-nav">
					<li><a href="#Home">Game</a>
					<li><a href="#Statistics">Highscores</a>
					
				</ul>
			</div>
		</div>
	</nav>
	
	<div class="jumbotron" id = "Home">
		<div class="container text-center" >
		<br>
	
		</div>
	<div>
  </body>
	<section>
	
	<div class="container text-center">
      <canvas class="emscripten" id="canvas" oncontextmenu="event.preventDefault()" height="700px" width="960px"></canvas>
      <br>
 
    </div>
    <script type='text/javascript'>
		var Module = {
    TOTAL_MEMORY: 268435456,
    errorhandler: null,			// arguments: err, url, line. This function must return 'true' if the error is handled, otherwise 'false'
    compatibilitycheck: null,
    backgroundColor: "#222C36",
    splashStyle: "Light",
    dataUrl: "Release/last.data",
    codeUrl: "Release/last.js",
    asmUrl: "Release/last.asm.js",
    memUrl: "Release/last.mem",
  };
	</script>
	<script src="Release/UnityLoader.js"></script>
	</div>
	</div>
	</section>
	
	
	<div class="container">
		<section>
			<div class="page-header" id="Statistics"> 
				<h3>Highscores:</h3>
								
				refreshTable();
														
		</section>	
	</div> 
	
	
	<footer>
	<div class="container-fluid">
		<div class = "container text-center">
		
		<p> Andre' Marmara' 2017 </p>			
		</div>
	</div>
	</footer>
	
	<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
		  refreshTable();
		});

		function refreshTable(){
			$('#Statistics').load('updateWebTable.php', function(){
			   setTimeout(refreshTable, 5000);
			});
		}
	</script>
</body>
</html>