﻿using UnityEngine;
using System.Collections;

public class Spike : MonoBehaviour {

    private Player player;
    bool invincible = false;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>(); // getting Player script
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator waitInvincible()
    {
        yield return new WaitForSeconds(2);
        invincible = false;
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (!invincible)
        {
            if (col.CompareTag("Player"))
            {
                takedamage();       
            }
            
        }
 
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (!invincible)
        {
            if (col.CompareTag("Player"))
            {
                takedamage();
            }

        }
    }

    void takedamage()
    {
        invincible = true;
        player.Damage(1);
        player.Knockback();

        StartCoroutine(waitInvincible());
        player.points -= 5;
    }

}



