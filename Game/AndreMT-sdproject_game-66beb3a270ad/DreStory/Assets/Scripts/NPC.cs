﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NPC : MonoBehaviour {

    public GameObject chatPanel;
	// Use this for initialization
	void Start () {
	
	}	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            chatPanel.SetActive(true);          
        }
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            chatPanel.SetActive(true);          
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        chatPanel.SetActive(false);

    }
}
