﻿using UnityEngine;
using System.Collections;

public class CameraPlayer : MonoBehaviour {


    private Vector2 velocity; // required for the script to work

    public float timeY;
    public float timeX;

    public GameObject player;

    public bool bounderies;
    public Vector3 minCam;
    public Vector3 maxCam;

	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player");
	}
	

    void FixedUpdate()
    {
        float X = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref velocity.x, timeX);
        float Y = Mathf.SmoothDamp(transform.position.y, player.transform.position.y, ref velocity.y, timeY);

        transform.position = new Vector3(X, Y, transform.position.z); // so it wont effect the z 

        if(bounderies)
        {
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, minCam.x, maxCam.x),
                Mathf.Clamp(transform.position.y, minCam.y, maxCam.y),
                Mathf.Clamp(transform.position.z, minCam.z, maxCam.z));
        }
    }
}
