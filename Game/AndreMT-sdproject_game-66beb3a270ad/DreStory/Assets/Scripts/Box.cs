﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour {

    public GameObject clue;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            clue.SetActive(true);
        }
    }
    
    public void OnTriggerExit2D(Collider2D col)
    {
        clue.SetActive(false);
        Destroy(this.gameObject);
    }
}
