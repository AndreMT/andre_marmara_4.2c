# README #


### SETUP ###
This project uses XAMPP for the local MySQL & Apache instance. 
Database file is found in the database folder which can be imported through PHPmyAdmin from XAMPP MySQL "Admin" button. Import of this .sql file will load all the tables required for the application to run. 

to run this on localhost, one must go to his browser of choice and type: http://localhost/world/index.php

It is important to put all of the folders and php files in the XAMPP "htdocs" file, else the website will not load.